#DST_DIR=$$top_builddir
#NV_ARCH=unknown

#win32{
#        NV_ARCH=windows
#        COPY_FILES = $$PWD/lib/$$NV_ARCH/*.dll
#        LIBS += -L$$PWD/lib/$$NV_ARCH -lvisa32
#} else {
#    unix {
#        NV_ARCH=linux
#        COPY_FILES = $$PWD/lib/$$NV_ARCH/*.so
#        LIBS += -L$$PWD/lib/$$NV_ARCH -lvisa
#        }
#	LIBS += -L$$PWD/lib/$$NV_ARCH
#}

message("NI VISA in " $$PWD/lib/$$NV_ARCH)


INCLUDEPATH += $$PWD/include/

#contains(QMAKE_HOST.os,Windows): {
#    COPY_FILES ~= s,/,\\,g
#    DST_DIR ~= s,/,\\,g
#}



#QMAKE_POST_LINK += $$escape_expand(\n\t) $$QMAKE_COPY $$quote($$COPY_FILES) $$quote($$DST_DIR)

DISTFILES += \
    $$PWD/README.md

#LIBS += -L$$PWD/msc/ -lvisa32
#LIBS += -L$$PWD/msc/ -lvisa64

