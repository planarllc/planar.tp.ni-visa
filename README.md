NI-VISA(TM) Software for Linux, Version 17.0
May 2017

---

[TOC]

---

# SYSTEM REQUIREMENTS

NI-VISA 17.0 for Linux software requires one of the following distributions:  

* openSUSE LEAP 42.1;  
* openSUSE LEAP 42.2;  
* Red Hat Enterprise Linux Desktop + Workstation 6;  
* Red Hat Enterprise Linux Desktop + Workstation 7;  
* Scientific Linux 6;  
* CentOS 7.  


You must use the provided INSTALL script to install NI-VISA. Manual
installation using rpm is not supported.

At the time of release, NI supports NI-VISA 17.0 on only the above listed 
distributions and versions. For information about NI-VISA compatibility 
on other versions of these distributions, including earlier versions 
of the distributions listed above, refer to KB 5CNEG20S on ni.com, 
"What Linux distributions do NI's drivers and software support?".

After installation, you may need to reboot your machine before attempting
to use NI-VISA. The installer will prompt you to reboot if it is required.

Refer to ni.com/linux for the most recent information about 
Linux support at NI.

# INSTALLATION FROM THE INTERNET

You should be logged in as 'root' to perform this installation.

1) Download the ISO image "NI-VISA-#.#.#.iso" from the ftp site into a
   temporary directory.

2) Open a Terminal (command prompt) window.

3) Mount the ISO image. This may be done by typing
   "mount -o loop <iso-location>/NI-VISA-#.#.#.iso <mount-location>".

4) Make the mount location your current working directory. This may be done
   by typing "cd <mount-location>".

5) Type "./INSTALL" to execute the install script, and follow the instructions
   as prompted.

# INSTALLATION FAQS

Q1: What action needs to be taken if I upgrade/change my kernel?

A1: Some action is required to make the existing NI-VISA installation
    work for the new kernel. After upgrading your kernel, run
    updateNIDrivers utility as root. The utility is located in
    /usr/local/bin. Be sure to have sources for your new kernel properly
    installed on your system before running the utility. The utility
    rebuilds the driver using the new kernel sources and copies it to
    the appropriate location for the new kernel. For the changes
    to take place, you will be asked to reboot your machine after the
    utility completes.

Q2: During installation I get a message similar to the following one:
    "Kernel source does not appear to be configured for the running kernel.
     Configuration of kernel source is required to continue installation."
    I do have kernel sources installed; is there anything else that needs to
    be done?

A2: This problem has been seen on SUSE LINUX Professional 9.1 Running the 2.6.5
kernel or newer versions of SUSE LINUX Professional. However, it is possible 
that other distributions will require similar steps to resolve this problem. 

	On SUSE LINUX Professional complete the following steps:
    1. Ensure you have installed kernel-source and kernel-syms packages
       corresponding to the version of the currently running kernel. The
       version of the currently running kernel can be determined by 
       issuing the command `uname -r`.
    2. Change the directory to the /usr/src/linux-<version> directory, where
       <version> corresponds to the currently running kernel version.
    3. Run "zcat /boot/symvers-<version>.gz > Module.symvers" as root to prepare
       modversion support.
    4. Run "make cloneconfig" as root to configure the sources for the
       currently running kernel.
    5. Run "make modules_prepare" as root to prepare the headers for
       compilation.
    6. THIS STEP IS STRICTLY OPTIONAL. Completing this step removes the
       warning:
       WARNING: Symbol version dump /usr/src/linux/Module.symvers is missing,
                modules will have CONFIG_MODVERSIONS disabled.
       Run "make modules" as root to compile all modules and generate
       Module.symvers; this step may take 30 minutes or more to complete.
    7. Run the INSTALL script for the NI-VISA software for Linux from this
       directory.


# USB ISSUES ON LINUX

The VISA user must have write access to the file that represents
the USB device, which is typically somewhere in a subdirectory within
"/dev/bus/usb". If this is not the case, the USB device is not
accessible by VISA (it will not be found using viFindRsrc and viOpen will
fail). The default configuration on most systems is that the 'root' user
has write access; however, no other user has this access.

There are a number of options that you can take to provide a non-root
user access to a USB device.

1) Use udev rules. By default, NI-VISA installs
scripts to give all users write access to all USB TMC devices and a
framework for USB RAW devices. To add write permissions for a specific
USB RAW device, run the included script:
"<VXIPNPPATH>/linux/NIvisa/USB/AddUsbRawPermissions.sh".
For more information about udev, please visit:
https://www.kernel.org/pub/linux/utils/kernel/hotplug/udev/udev.html


2) The 'root' user may add write permissions to the file that represents
the USB device which is typically somewhere in a subdirectory within
"/dev/bus/usb". Unfortunately, these permissions will be lost if the
device is unplugged and then plugged back in. Because of this, this
approach is not recommended.

# KNOWN ISSUES AND BUG FIXES

For a list of Known Issues of NI-VISA 17.0, visit ni.com/info and
enter the Info Code NIVISA170KnownIssues.

For a list of bug fixes for NI-VISA, visit ni.com/info and enter
the Info Code NIVISAFixList.

# NEW FEATURES AND IMPROVEMENTS

## NI-VISA 17.0

Added support for NI System Configuration API.
Added NI I/O Trace support for 64-bit VISA applications.

## NI-VISA 15.0

Added support for developing 64-bit VISA applications.

## NI-VISA 14.0

Added support for viGpibControlREN on TCPIP resources that use the 
VXI-11 protocol.

## NI-VISA 5.3

Improved performance of viRead for a HiSLIP resource.

## NI-VISA 5.2

Improved performance of viWrite for a HiSLIP resource.

## NI-VISA 5.1

Added support for HiSLIP.

Updated the VISA Interactive Control stand-alone application. Refer to the
NI-VISA Help for more information on the updates to VISAIC.

## NI-VISA 5.0

Added support for openSUSE 11.1, openSUSE 11.2, and Scientific Linux 5.x

Added support for 64-bit formatted I/O.

Dropped support for GPIB-VXI.


# LEGAL INFORMATION
## COPYRIGHT:

(c) 2017 National Instruments.
All rights reserved.

Under the copyright laws, this publication may not be reproduced or transmitted
in any form, electronic or mechanical, including photocopying, recording,
storing in an information retrieval system, or translating, in whole or in
part, without the prior written consent of National Instruments Corporation.

National Instruments respects the intellectual property of others, and we ask
our users to do the same. NI software is protected by copyright and other
intellectual property laws. Where NI software may be used to reproduce
software or other materials belonging to others, you may use NI software
only to reproduce materials that you may reproduce in accordance with the
terms of any applicable license or other legal restriction.

Third-Party Legal Notices
You can find NI-VISA third-party legal notices in the 
/usr/local/vxipnp/share/nivisa directory. 

U.S. Government Restricted Rights
If you are an agency, department, or other entity of the United States
Government ("Government"), the use, duplication, reproduction, release,
modification, disclosure or transfer of the technical data included in this
manual is governed by the Restricted Rights provisions under Federal
Acquisition Regulation 52.227-14 for civilian agencies and Defense Federal
Acquisition Regulation Supplement Section 252.227-7014 and 252.227-7015 for
military agencies.

IVI Foundation Copyright Notice
Content from the IVI specifications reproduced with permission from the IVI
Foundation.

The IVI Foundation and its member companies make no warranty of any kind with
regard to this material, including, but not limited to, the implied warranties
of merchantability and fitness for a particular purpose. The IVI Foundation and
its member companies shall not be liable for errors contained herein or for
incidental or consequential damages in connection with the furnishing,
performance, or use of this material.

## TRADEMARKS:

Refer to the NI Trademarks and Logo Guidelines at ni.com/trademarks for
information on National Instruments trademarks. Other product and company names
mentioned herein are trademarks or trade names of their respective companies.

## PATENTS:

For patents covering the National Instruments products/technology, refer to the
appropriate location: Help�Patents in your software, the patents.txt file on
your media, or the National Instruments Patent Notice at ni.com/patents.

